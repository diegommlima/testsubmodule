//
//  AppDelegate.h
//  SubmoduleTest
//
//  Created by Diego Lima on 9/18/15.
//  Copyright © 2015 DiegoLima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

